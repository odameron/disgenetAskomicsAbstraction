# disgenetAskomicsAbstraction

[DisGeNet](https://www.disgenet.org/) is a collection of genes and variants associated to human diseases.
The current version of DisGeNET (v7.0) contains 1,134,942 gene-disease associations (GDAs), between 21,671 genes and 30,170 diseases, disorders, traits, and clinical or abnormal human phenotypes, and 369,554 variant-disease associations (VDAs), between 194,515 variants and 14,155 diseases, traits, and phenotypes.
[DisGeNET is also available in RDF](https://www.disgenet.org/rdf), and has a public SPARQL endpoint at [http://rdf.disgenet.org/sparql/](http://rdf.disgenet.org/sparql/).

This project generates the abstraction file for using [AskOmics](https://askomics.org/) as an user-friendly front-end for designing SPARQL queries visually.
